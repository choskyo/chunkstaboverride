using Vintagestory.API.Common;

[
    assembly:
        ModInfo("Chunk Stability Override", "chunkstaboverride",
        Version = "2.0.1",
        Authors = new[] { "Cho" },
        Website = "",
        Description = "Adds server commands to increase/decrease temporal stability globally or per-chunk",
        RequiredOnClient = false,
        Side = "Universal")
]
