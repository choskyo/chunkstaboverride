﻿using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.GameContent;
using Vintagestory.ServerMods;

namespace StabMod;

public class StabModServerModSystem : ModSystem
{
    private IServerNetworkChannel? NetworkChannel { get; set; }
    private ICoreServerAPI? Api { get; set; }
    private SystemTemporalStability? StabSystem { get; set; }
    private Dictionary<IServerChunk, float> ModdedChunks { get; set; } = new();
    private float GlobalStabMod;

    public override bool ShouldLoad(EnumAppSide forSide)
        => forSide == EnumAppSide.Server;

    public override void StartServerSide(ICoreServerAPI api)
    {
        Api = api;
        StabSystem = api.ModLoader.GetModSystem<SystemTemporalStability>();

        api.RegisterCommand
        (
            "globalStab",
            "Modifies stability in all chunks by 'modBy'. When this is != 0, local chunk changes are ignored.",
            "/globalStab modBy",
            GlobalStabHandler,
            api.Server.IsDedicated ? Constants.Privilege : Vintagestory.API.Server.Privilege.chat
        );

        api.RegisterCommand
        (
            "stab",
            "Modifies stability in current chunk by 'modBy', unless a global modifier is set.",
            "/stab modBy",
            StabHandler,
            api.Server.IsDedicated ? Constants.Privilege : Vintagestory.API.Server.Privilege.chat
        );

        api.RegisterCommand
        (
            "checkStab",
            "Stab checker",
            "/checkStab",
            CheckStabHandler,
            Vintagestory.API.Server.Privilege.chat
        );

        StabSystem.OnGetTemporalStability += GetTemporalStabilityOverride;

        api.Event.ChunkColumnLoaded += OnChunkLoaded;
        api.Event.SaveGameLoaded += OnSaveGameLoading;
        api.Event.GameWorldSave += OnSaveGameSaving;
    }

    private float GetTemporalStabilityOverride(float originalStability, double x, double y, double z)
    {
        if (StabSystem!.StormData.nowStormActive)
        {
            return originalStability;
        }

        var chunkStabMod = TryGetChunkMod(x, y, z);

        if (chunkStabMod != 0)
        {
            return originalStability + chunkStabMod;
        }

        return originalStability + GlobalStabMod;
    }

    private float TryGetChunkMod(double x, double y, double z)
    {
        try
        {
            var chunkPosition = new BlockPos((int)x, (int)y, (int)z);
            var chunk = Api!.WorldManager.GetChunk(chunkPosition);

            if (chunk is null)
            {
                Api!.Logger.Warning($"Got null chunk at X{(int)x}Y{(int)y}Z{(int)z}. Returning 0 for modifier.");
                return 0;
            }

            ModdedChunks.TryGetValue(chunk, out var stabMod);

            return stabMod;
        }
        catch (Exception ex)
        {
            Api!.Logger.Error("Unable to get the chunk's stability modifier, if you were expecting one, the log following this should be the error I'll need to debug it");
            Api!.Logger.Error(ex);

            return 0;
        }
    }

    private void CheckStabHandler(IServerPlayer player, int groupId, CmdArgs args)
    {
        var globalUsed = false;
        var localUsed = false;
        var msg = "";

        if (GlobalStabMod is not 0)
        {
            globalUsed = true;
            msg += $"Stability is being globally modified by {GlobalStabMod}. ";
        }

        var playerPos = player.Entity.Pos.AsBlockPos;
        var chunkStab = TryGetChunkMod(playerPos.X, playerPos.Y, playerPos.Z);

        if (chunkStab != 0)
        {
            localUsed = true;
            msg += $"Stability is being locally modified by {chunkStab}. ";
        }

        if (globalUsed && localUsed)
        {
            msg += $"Using {chunkStab}.";
        }

        if (!globalUsed && !localUsed)
        {
            msg = "Stability is not being modified";
        }

        Api!.SendMessage
        (
            player,
            groupId,
            msg,
            EnumChatType.CommandSuccess
        );
    }

    private void GlobalStabHandler(IPlayer player, int groupId, CmdArgs args)
    {
        var mod = TryGetModBy(player, groupId, args);
        if (mod is null)
        {
            return;
        }

        GlobalStabMod = mod.Value;

        SaveGlobalStabMod();
    }

    private float? TryGetModBy(IPlayer player, int groupId, CmdArgs args)
    {
        var mod = args.PopFloat();

        if (!mod.HasValue)
        {
            Api!.SendMessage
            (
                player,
                groupId,
                $"Missing required mod value, got: {args.PeekWord()}",
                EnumChatType.CommandError
            );

            return null;
        }

        return mod;
    }

    private void StabHandler(IPlayer player, int groupId, CmdArgs args)
    {
        var mod = TryGetModBy(player, groupId, args);
        if (mod is null)
        {
            return;
        }

        var chunk = Api!.WorldManager.GetChunk(player.Entity.Pos.AsBlockPos);

        if (mod == 0)
        {
            ModdedChunks.Remove(chunk);
            chunk.SetModdata(Constants.ModDataKey, Array.Empty<byte>());

            return;
        }

        ModdedChunks[chunk] = mod.Value;
        chunk.SetModdata(Constants.ModDataKey, SerializerUtil.Serialize(mod));

        Api.SendMessage(player, groupId, $"Set chunk stability override to {mod}", EnumChatType.CommandSuccess);
    }

    private void OnChunkLoaded(Vec2i _, IWorldChunk[] chunks)
    {
        foreach (var worldChunk in chunks)
        {
            var chunk = (IServerChunk)worldChunk;
            var savedData = chunk.GetModdata(Constants.ModDataKey);

            if (savedData is null || savedData.Length == 0)
            {
                continue;
            }

            var value = SerializerUtil.Deserialize<float>(savedData);

            if (value != 0)
            {
                ModdedChunks.TryAdd(chunk, value);
            }
        }
    }

    private void OnSaveGameSaving()
    {
        ModdedChunks
            .Where(chunk => chunk.Value != 0)
            .ToList()
            .ForEach(chunk => chunk.Key.SetModdata(Constants.ModDataKey, SerializerUtil.Serialize(chunk.Value)));

        SaveGlobalStabMod();
    }

    private void SaveGlobalStabMod()
    {
        if (GlobalStabMod == 0 && Api!.World.Config.HasAttribute(Constants.ModDataKey))
        {
            Api.World.Config.RemoveAttribute(Constants.ModDataKey);
        }

        if (GlobalStabMod != 0)
        {
            Api!.World.Config.SetFloat(Constants.ModDataKey, GlobalStabMod);
        }
    }

    private void OnSaveGameLoading()
    {
        ModdedChunks = new Dictionary<IServerChunk, float>();

        GlobalStabMod = Api!.World.Config.HasAttribute(Constants.ModDataKey)
            ? Api.World.Config.GetFloat(Constants.ModDataKey)
            : 0;
    }
}
