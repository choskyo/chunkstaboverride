namespace StabMod;

public static class Constants
{
    public const string Privilege = "stab";
    public const string ModDataKey = "stabilityModifier"; 
}
